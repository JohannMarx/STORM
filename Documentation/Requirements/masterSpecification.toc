\contentsline {section}{\numberline {1}Vision and Scope}{3}{section.1}
\contentsline {subsection}{\numberline {1.1}History and Background}{3}{subsection.1.1}
\contentsline {subsubsection}{\numberline {1.1.1}A Brief History of Project STORM}{3}{subsubsection.1.1.1}
\contentsline {subsubsection}{\numberline {1.1.2}Project Background}{3}{subsubsection.1.1.2}
\contentsline {subsection}{\numberline {1.2}Project Scope}{3}{subsection.1.2}
\contentsline {section}{\numberline {2}Application requirements and design}{3}{section.2}
\contentsline {subsection}{\numberline {2.1}Modular Design}{3}{subsection.2.1}
\contentsline {subsection}{\numberline {2.2}User Access Module}{4}{subsection.2.2}
\contentsline {subsubsection}{\numberline {2.2.1}Use-cases}{5}{subsubsection.2.2.1}
\contentsline {subsection}{\numberline {2.3}Project Module}{8}{subsection.2.3}
\contentsline {subsubsection}{\numberline {2.3.1}Use-cases}{8}{subsubsection.2.3.1}
\contentsline {subsection}{\numberline {2.4}Database Interaction Module}{9}{subsection.2.4}
\contentsline {subsubsection}{\numberline {2.4.1}Use-cases}{10}{subsubsection.2.4.1}
\contentsline {subsection}{\numberline {2.5}Algorithm Module}{11}{subsection.2.5}
\contentsline {subsubsection}{\numberline {2.5.1}Use-cases}{12}{subsubsection.2.5.1}
\contentsline {subsection}{\numberline {2.6}Reporting Module}{13}{subsection.2.6}
\contentsline {section}{\numberline {3}Architectural Requirements}{13}{section.3}
\contentsline {subsection}{\numberline {3.1}Access and Integration Requirements}{13}{subsection.3.1}
\contentsline {subsubsection}{\numberline {3.1.1}Human Access Channels}{13}{subsubsection.3.1.1}
\contentsline {subsubsection}{\numberline {3.1.2}System Access Channels}{13}{subsubsection.3.1.2}
\contentsline {subsection}{\numberline {3.2}Quality Requirements}{13}{subsection.3.2}
\contentsline {subsubsection}{\numberline {3.2.1}Usability}{14}{subsubsection.3.2.1}
\contentsline {subsubsection}{\numberline {3.2.2}Scalability}{14}{subsubsection.3.2.2}
\contentsline {subsubsection}{\numberline {3.2.3}Performance}{14}{subsubsection.3.2.3}
\contentsline {subsubsection}{\numberline {3.2.4}Maintainability}{14}{subsubsection.3.2.4}
\contentsline {subsubsection}{\numberline {3.2.5}Reliability}{15}{subsubsection.3.2.5}
\contentsline {subsubsection}{\numberline {3.2.6}Deployability}{15}{subsubsection.3.2.6}
\contentsline {subsubsection}{\numberline {3.2.7}Security}{15}{subsubsection.3.2.7}
\contentsline {subsubsection}{\numberline {3.2.8}Testability}{15}{subsubsection.3.2.8}
\contentsline {subsection}{\numberline {3.3}Architectural Responsibilities}{16}{subsection.3.3}
\contentsline {subsection}{\numberline {3.4}Architecture Constraints}{16}{subsection.3.4}
